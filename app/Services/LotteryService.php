<?php

namespace App\Services;

use phpDocumentor\Reflection\Types\This;

class LotteryService
{
    public $number = 0;
    public $lucky = 'Lose';
    public $coefficient = 0;
    public $adword = 0;

    public function __construct()
    {
        $this->number = rand(0, 1000);
    }

    /**
     * @return string
     */
    public function lucky()
    {
       $this->lucky =  $this->number  % 2 === 0  ? 'Win' : 'Lose';
       return $this;
    }

    /**
     * @param $number
     * @param $coefficient
     * @return float|int
     */
    protected function partOfTheWin($number, $coefficient)
    {
        return  ($number * $coefficient)/100;
    }

    /**
     * @return mixed
     */
    public function  addCoefficient()
    {

        switch ($this->number)
        {
            case ($this->number>900):
                $coefficient  = 70;
                break;

            case ($this->number>600 && $this->number<900):
                $coefficient  = 50;
                break;

            case ($this->number>300 && $this->number<600):
                $coefficient  = 30;
                break;

            case ( $this->number<300):
                $coefficient  = 10;
                break;
            default:
                $coefficient  = 0;
        }

       $this->coefficient = $coefficient;

        return $this;
    }

    public function adword()
    {
        $this->adword =  $this->partOfTheWin($this->number, $this->coefficient);

        return $this;

    }


    public  function getLotteryResult()
    {

        return [
            'number' => $this->number,
            'lucky' => $this->lucky,
            'adword' => $this->adword,
        ];
    }


}
