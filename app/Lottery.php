<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lottery extends Model
{
    protected $table = 'lotteries';

    protected $fillable = [
        'user_id',
        'number',
        'adword',
        'lucky'
    ];
}
