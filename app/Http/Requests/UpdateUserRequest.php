<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'phone' => 'regex:/(38)[0-9]{9}/|unique:users'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'phone.unique:users' => 'This phone is exists!',
            'phone.regex:/(01)[0-9]{9}/' => 'The phone format is wrong!',
        ];
    }
}
