<?php

namespace App\Http\Controllers;

use App\Lottery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class HistoryController extends Controller
{
    public function index()
    {
        $history = Lottery::where('user_id', Session::get('id'))
            ->take(3)
            ->latest()
            ->get();
       
        return view('history.index', compact('history'));
    }
}
