<?php

namespace App\Http\Controllers;

use App\Lottery;
use App\Services\LotteryService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Session;

class LotteryController extends Controller
{
    public function index()
    {
        $lottery = app(LotteryService::class)
            ->lucky()
            ->addCoefficient()
            ->adword()
            ->getLotteryResult();

        $lottery = Arr::add($lottery, 'user_id', Session::get('id'));


        Lottery::create($lottery);

        return view('lottery.index', $lottery);
    }
}
