<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
   public function index(Request $request)
   {
       $user = User::wherePhone($request->get('phone'))->firstOrFail();

       return view('dashboard.index', ['data' => $user->invitations()->first()]);
   }
}
