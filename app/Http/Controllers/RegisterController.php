<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Invitation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class RegisterController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('registration.create');
    }

    /**
     * @param Request $request
     */
    public function store(RegistrationRequest $request)
    {

        $params = $request->only(['name', 'phone']);

        $url = URL::temporarySignedRoute('dashboard', now()->addDays(7), $params);

        if ($user = User::create($params)) {

             Invitation::create([
                    'user_id' => $user->id,
                    'url' => $url
                ]
            );
            Session::put('id',$user->id);
            return redirect('registration/link')->with(compact('url'));

        }
        return redirect()->back()->withErrors('Something went wrong!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('registration.show');
    }
}
