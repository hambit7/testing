@extends('layouts.app')

@section('content')

    <button class="btn btn-success"><a href="{{url('admin/users/create') }}">Add user</a></button>
    <div class="card-header">
        <h3 class="card-title">Users</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th style="width: 10px">#</th>
                <th>Name</th>
                <th>Phone</th>
                <th style="width: 40px">Cretated</th>
                <th style="width: 40px">Operations</th>
            </tr>
            @forelse($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>
                    {{ $user->phone }}
                </td>
                <td>{{ $user->created_at }}</td>
                <td>
                    <a href="{{ url('admin/users/'. $user->id) }}" type="button" class="btn btn-default">View</a>
                    <br>
                    <a href="{{ url('admin/users/'. $user->id .'/edit') }}" type="button" class="btn btn-default">Edit</a>
                    <br>
                    <form role="form" action="{{url('/admin/users/' . $user->id)}}" method="POST">
                        @csrf
                        @method('DELETE')

                                <button type="submit" class="btn btn-danger">DELETE</button>

                    </form>

                </td>
            </tr>
            @empty
            <p>No users</p>
                @endforelse
        </table>
    </div>
    <!-- /.card-body -->

@endsection

