@extends('layouts.app')

@section('content')
    <h1>creare new user</h1>
    <form role="form" action="{{url('/admin/users')}}" method="POST">
        @csrf
        @include('admin.users.form',['user' => new App\User])

        <div class="field">
            <div class="control">
                <button type="submit" class="btn btn-success">Create a user</button>
                <a href="/admin/users">Cancel</a>
            </div>
        </div>
    </form>

    @endsection
