@extends('layouts.app')

@section('content')
    <div class="card-header">
        <h3 class="card-title">User # {{ $user->id }}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
          <br>
     Name:   {{ $user->name }}
        <br>
     Phone:   {{ $user->phone }}
    </div>

@endsection
