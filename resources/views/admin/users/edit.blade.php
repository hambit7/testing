@extends('layouts.app')

@section('content')
    <h1>update user #  {{ $user->id }}</h1>
    <form role="form" action="{{url('/admin/users/' . $user->id)}}" method="POST">
        @csrf
        @method('PATCH')
        @include('admin.users.form',['user' => $user])

        <div class="field">
            <div class="control">
                <button type="submit" class="btn btn-success">Update a user</button>
                <a href={{url('/admin/users/' . $user->id . '/edit')}}   >Cancel</a>
            </div>
        </div>
    </form>

@endsection
