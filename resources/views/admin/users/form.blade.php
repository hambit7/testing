@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="error text-danger">{{$error}}</div>
    @endforeach
@endif

<div class="row">

        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" name="name" value="{{$user->name}}" class="form-control" id="exampleInputEmail1" placeholder="Enter name">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Phone</label>
                <input type="text" name="phone" value="{{$user->phone}}"  class="form-control" id="exampleInputPassword1" placeholder="Phone">
            </div>
        </div>
        <!-- /.card-body -->

</div>
