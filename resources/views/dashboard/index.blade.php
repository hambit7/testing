@extends('layouts.app')

@section('content')
    Your link is   - {{  $data->url }}
    <br>
    <form class="form-group"
          action="{{url('invitation')}}" method="POST">
        @method('DELETE')
        @csrf

        <input type="text" name="url" value="{{ $data->url }}" id="copy-url" hidden>
        <div>
            <button type="submit" class="btn btn-danger">DELETE</button>
        </div>
    </form>
   <div>
    <button class="btn btn-primary" onclick="copyUrl()">Copy url</button>
   </div>
    <br>
    <form class="form-group"
          action="{{url('invitation')}}" method="POST">
        @method('POST')
        @csrf

        <input type="text" name="url" value="{{ $data->url }}" id="copy-url" hidden>
        <div>
            <button type="submit" class="btn btn-success">GENERATE NEW URL</button>
        </div>
    </form>

    <a href="{{url('lottery')}}">I am feeling lucky</a><br>
    <a href="{{url('history')}}">History</a>

    <script>
        function copyUrl() {
            /* Get the text field */
            var copyText = document.getElementById('copy-url');
            console.log(copyText);
            copyText.select();

            /* Copy the text inside the text field */
            document.execCommand("copy");

            /* Alert the copied text */
            alert("Copied the text: " + copyText.value);
        }

    </script>
@endsection

