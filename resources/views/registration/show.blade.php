@extends('layouts.app')

@section('content')
    @if (session('url'))
        <div class="alert alert-success">
            Your link is      {{ session('url') }}
        </div>
    @endif
@endsection
