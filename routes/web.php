<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/registration', 'RegisterController@create');
Route::get('/lottery', 'LotteryController@index');
Route::get('/history', 'HistoryController@index');
Route::post('/registration', 'RegisterController@store');
Route::post('/invitation', 'InvitationController@update');
Route::delete('/invitation', 'InvitationController@destroy');
Route::get('/registration/link', 'RegisterController@show');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('signed');

Route::prefix('admin')->group(function () {
    Route::resource('/users',  'Admin\UsersController');
});



